#pragma once
#include "stdint.h"

struct pixel
{
    uint8_t b, g, r;
} __attribute__((packed));

struct image
{
    uint64_t width, height;
    struct pixel* data;
} __attribute__((packed));

struct image
create_image(uint64_t const width, uint64_t const height);

void
free_image(struct image* image);
