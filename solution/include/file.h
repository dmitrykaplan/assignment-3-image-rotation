#include <stdbool.h>
#include <stdio.h>

FILE*
get_read_file(const char* path);

FILE*
get_write_file(const char* path);

bool
release_file(FILE* file);
