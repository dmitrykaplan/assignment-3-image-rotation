#include "../include/transform.h"
#include "stdio.h"

struct image
rotate(struct image const source)
{
    struct image result = create_image(source.height, source.width);
    for (size_t y = 0; y < result.height; y++) {
        for (size_t x = 0; x < result.width; x++) {
            result.data[y * result.width + x] =
                    source.data[(source.height - x) * source.width + y - source.width];
        }
    }
    return result;
}
