#include "file.h"

#include <stdio.h>

FILE*
get_read_file(const char* path)
{
    return fopen(path, "r+");
}

FILE*
get_write_file(const char* path)
{
    return fopen(path, "w+");
}

bool
release_file(FILE* file)
{
    if (fclose(file) == 0) {
        return true;
    } else {
        return false;
    }
}
