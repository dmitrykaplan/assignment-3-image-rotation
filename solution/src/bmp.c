#include "bmp.h"
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>

#define BMP_SIGNATURE 0x4D42
#define BMP_SUPPORTED_HEADER_SIZE 40
#define BMP_REQUIRED_PLANES 1
#define BMP_VERIFY_BIT_COUNT 24
#define BMP_VERIFY_COMPRESSION 0

struct bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
} __attribute__((packed));

static uint8_t
get_padding(uint64_t width)
{
    return width % 4;
}

static uint64_t
bmp_array_size(uint64_t const width, uint64_t const height)
{
    return width * height * sizeof(struct pixel) + get_padding(width) * height;
}

enum read_status
from_bmp(FILE* file, struct image* img)
{
    fseek(file, 0, SEEK_SET);
    char scratch_buffer[32];
    struct bmp_header header = { 0 };

    if (read(file->_fileno, &header, sizeof(struct bmp_header)) !=
        sizeof(struct bmp_header)) {
        return READ_UNSUCCESSFUL;
    }

    if (header.bfType != BMP_SIGNATURE) {
        return READ_NOT_BMP;
    }
    if (header.biBitCount != BMP_VERIFY_BIT_COUNT) {
        return READ_BMP_NOT_SUPPORTED;
    }
    if (header.biSize != BMP_SUPPORTED_HEADER_SIZE) {
        return READ_BMP_NOT_SUPPORTED;
    }
    if (header.biCompression != BMP_VERIFY_COMPRESSION) {
        return READ_BMP_NOT_SUPPORTED;
    }

    const uint64_t padding_size = get_padding(header.biWidth);
    *img = create_image(header.biWidth, header.biHeight);
    for (size_t r = 0; r < img->height; r++) {
        if (fread(img->data + r * img->width,
                  img->width * sizeof(struct pixel),
                  1,
                  file) != 1)
            return READ_UNSUCCESSFUL;
        if (fread(scratch_buffer, padding_size, 1, file) != 1)
            return READ_UNSUCCESSFUL;
    }
    return READ_OK;
}

enum write_status
to_bmp(FILE* file, struct image const* img)
{
    fseek(file, 0, SEEK_SET);
    const char zeros[128] = { 0 };
    const uint8_t padding = get_padding(img->width);
    const uint64_t imageSize = bmp_array_size(img->width, img->height);

    struct bmp_header header = { 0 };
    header.bfType = BMP_SIGNATURE;
    header.bfileSize = (uint32_t)(sizeof(struct bmp_header) + imageSize);
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = BMP_SUPPORTED_HEADER_SIZE;
    header.biWidth = (uint32_t)img->width;
    header.biHeight = (uint32_t)img->height;
    header.biPlanes = BMP_REQUIRED_PLANES;
    header.biBitCount = BMP_VERIFY_BIT_COUNT;
    header.biSizeImage = (uint32_t)imageSize;

    if (write(file->_fileno, &header, sizeof(struct bmp_header)) !=
        sizeof(struct bmp_header)) {
        puts("Something went bad");
        exit(1);
        return WRITE_ERROR;
    }
    fsync(file->_fileno);

    for (size_t i = 0; i < img->height; i++) {
        if (fwrite(img->data + i * img->width,
                   img->width * sizeof(struct pixel),
                   1,
                   file) != 1) {
            return WRITE_ERROR;
        }
        if (fwrite(zeros, padding, 1, file) != 1) {
            return WRITE_ERROR;
        }
    }
    fsync(file->_fileno);

    return WRITE_OK;
}
