#include "bmp.h"
#include "file.h"
#include "transform.h"
#include <stdlib.h>

static void
crash(const char* message)
{
    puts(message);
    exit(1);
}

int
main(int argc, char** argv)
{
    if (argc != 3) {
        crash("Wrong usage");
    }

    FILE* file_in = get_read_file(argv[1]);
    FILE* file_out = get_write_file(argv[2]);

    if (file_in == NULL) {
        crash("Input file inaccessible");
    }

    if (file_out == NULL) {
        crash("Output file inaccessible");
    }
    struct image img = { 0 };
    enum read_status const read_status = from_bmp(file_in, &img);

    switch (read_status) {
        case READ_OK:
            break;
        case READ_UNSUCCESSFUL:
            crash("Unable to read the file");
            break;
        case READ_NOT_BMP:
            crash("File not a BMP file");
            break;
        case READ_BMP_NOT_SUPPORTED:
            crash("BMP file not supported");
            break;
    }

    struct image rotated = rotate(img);
    free_image(&img);

    enum write_status const write_status = to_bmp(file_out, &rotated);
    fflush(file_out);
    free_image(&rotated);

    if (write_status != WRITE_OK) {
        crash("File saving error");
    }

    printf("Image saved in %s\n", argv[2]);

    if (!release_file(file_in)) {
        crash("Failed to close read file");
    }
    if (!release_file(file_out)) {
        crash("Failed to close write file");
    }

    return 0;
}
